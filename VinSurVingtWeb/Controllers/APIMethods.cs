﻿// using Microsoft.VisualBasic.ApplicationServices;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VinSurVingtWeb.Models;

namespace VinSurVingtWeb.Controllers
{
    internal class APIMethods
    {
        //UpdateMode
        public static AutoUpdate GetAutoUpdate()
        {
            return APIController.GetAllReviews<AutoUpdate>("/AutoUpdate").Result.First(); // .First()?
        }
        public static bool SetAutoUpdate(AutoUpdate updateStatus)
        {
            return APIController.PutItem<AutoUpdate>(updateStatus, "/AutoUpdate/put", 1); // .First()?
        }
        //search
        public static List<Article> GetArticleBySearch(string search)
        {
            return APIController.GetReviewById<Article>("/Article/like", search).Result;
        }
        public static Users GetUsersByEmail(string email)
        {
            return APIController.GetReviewById<Users>("/Users/Email", email).Result.First();
        }

        public static List<Article> getArticleByProvider(int provider)
        {
            return APIController.GetReviewById<Article>("/Article/fournisseur", provider).Result;
        }

        public static List<Article> getArticleByResignation(string article)
        {
            return APIController.GetReviewById<Article>("/Article/articleResignation", article).Result;
        }

        public static Provider getProviderByName(string fournisseur)
        {
            return APIController.GetReviewById<Provider>("/Provider/Societe", fournisseur).Result.First();
        }
       
        public static Famille getFamilyByName(string famille)
        {
            return APIController.GetReviewById<Famille>("/Famille/Name", famille).Result.First();
        }

        public static List<Article> getArticleByFamille(int idFamille)
        {
            return APIController.GetReviewById<Article>("/Article/getArticleByFamille", idFamille).Result;
        }

        //GetAll
        public static List<Adresse> GetAllAdress()
        {   
            return APIController.GetAllReviews<Adresse>("/Adresse").Result;
        }
        public static List<AdresseClient> GetAllAdressClient()
        {
            return APIController.GetAllReviews<AdresseClient>("/AdresseClient").Result;
        }
        public static List<Article> GetAllArticle()
        {
            return APIController.GetAllReviews<Article>("/Article").Result;
        }
        public static List<Avis> GetAllAvis()
        {
            return APIController.GetAllReviews<Avis>("/Avis").Result;
        }
        public static List<Clients> GetAllClients()
        {
            return APIController.GetAllReviews<Clients>("/Clients").Result;
        }
        public static List<Commande> GetAllCommand()
        {
            return APIController.GetAllReviews<Commande>("/Commande").Result;
        }
        public static List<CommandeArticle> GetAllCommandArticle()
        {
            return APIController.GetAllReviews<CommandeArticle>("/CommandeArticle").Result;
        }
        public static List<Contact> GetAllContact()
        {
            return APIController.GetAllReviews<Contact>("/Contact").Result;
        }
        public static List<Famille> GetAllFamille()
        {
            return APIController.GetAllReviews<Famille>("/Famille").Result;
        }
        public static List<Gerant> GetAllGerant()
        {
            return APIController.GetAllReviews<Gerant>("/Gerant").Result;
        }
        public static List<Provider> GetAllFournisseur()
        {
            return APIController.GetAllReviews<Provider>("/Provider").Result;
        }
        public static List<Tva> GetAllTva()
        {
            return APIController.GetAllReviews<Tva>("/Tva").Result;
        }
        public static List<Users> GetAllUsers()
        {
            return APIController.GetAllReviews<Users>("/Users").Result;
        }

        //GetOne

        public static Gerant GetGerantByAuth(int auth)
        {
            return APIController.GetReviewById<Gerant>("/Gerant/authId", auth).Result.First();
        }

        public static Adresse GetAdressById(int id)
        {
            return APIController.GetReviewById<Adresse>("/Adresse", id).Result.First();
        }
        public static AdresseClient GetAdresseClientById(int id)
        {
            return APIController.GetReviewById<AdresseClient>("/AdresseClient", id).Result.First();
        }
        public static Article GetArticleById(int id)
        {
            return APIController.GetReviewById<Article>("/Article", id).Result.First();
        }

        public static Avis GetAvisById(int id)
        {
            return APIController.GetReviewById<Avis>("/Avis", id).Result.First();
        }
        public static Clients GetClientById(int id)
        {
            return APIController.GetReviewById<Clients>("/Clients", id).Result.First();
        }
        public static Commande GetCommandeById(int id)
        {
            return APIController.GetReviewById<Commande>("/Commande", id).Result.First();
        }

        public static List<Commande> GetLastCommand()
        {
            return APIController.GetAllReviews<Commande>("/Commande/GetLast").Result;
        }

        public static List<CommandeArticle> GetCommandeArticleById(int id)
        {
            return APIController.GetReviewById<CommandeArticle>("/CommandeArticle", id).Result;
        }
        public static Contact GetContactById(int id)
        {
            return APIController.GetReviewById<Contact>("/Contact", id).Result.First();
        }
        public static Famille GetFamilleById(int id)
        {
            return APIController.GetReviewById<Famille>("/Famille", id).Result.First();
        }
        public static Gerant GetGerantById(int id)
        {
            return APIController.GetReviewById<Gerant>("/Gerant", id).Result.First();
        }
        public static Provider GetProviderById(int id)
        {
            return APIController.GetReviewById<Provider>("/Provider", id).Result.First();
        }
        public static Tva GetTvaById(int id)
        {
            return APIController.GetReviewById<Tva>("/Tva", id).Result.First();
        }
        public static Users GetUserById(int id)
        {
            return APIController.GetReviewById<Users>("/Users", id).Result.First();
        }

        //delete
        public static bool DeleteAdressById(int id)
        {
            return APIController.DeleteItem("/Adresse/delete", id);
        }
        public static bool DeleteAdresseClientById(int id)
        {
            return APIController.DeleteItem("/AdresseClient/delete", id);
        }
        public static bool DeleteArticleById(int id)
        {
            return APIController.DeleteItem("/Article/delete", id);
        }
        public static bool DeleteAvisById(int id)
        {
            return APIController.DeleteItem("/Avis/delete", id);
        }
        public static bool DeleteClientById(int id)
        {
            return APIController.DeleteItem("/Clients/delete", id);
        }
        public static bool DeleteCommandeById(int id)
        {
            return APIController.DeleteItem("/Commande/delete", id);
        }
        public static bool DeleteCommandeArticleById(int id)
        {
            return APIController.DeleteItem("/CommandeArticle/delete", id);
        }
        public static bool DeleteContactById(int id)
        {
            return APIController.DeleteItem("/Contact/delete", id);
        }
        public static bool DeleteFamilleById(int id)
        {
            return APIController.DeleteItem("/Famille/delete", id);
        }
        public static bool DeleteGerantById(int id)
        {
            return APIController.DeleteItem("/Gerant/delete", id);
        }
        public static bool DeleteProviderById(int id)
        {
            return APIController.DeleteItem("/Provider/delete", id);
        }
        public static bool DeleteTvaById(int id)
        {
            return APIController.DeleteItem("/Tva/delete", id);
        }
        public static bool DeleteUserById(int id)
        {
            return APIController.DeleteItem("/Users/delete", id);
        }


        //Post (insert into)  APIController.PutItem(8, client, "/Clients/put");
        public static bool PostAdress(Adresse adresse)
        {
            return APIController.PostItem<Adresse>(adresse, "/Adresse/post");
        }
        public static bool PostAdresseClient(AdresseClient adresseClient)
        {
            return APIController.PostItem<AdresseClient>(adresseClient, "/AdresseClient/post");
        }
        public static bool PostArticle(Article article)
        {
            return APIController.PostItem<Article>(article, "/Article/post");
        }
        public static bool PostAvis(Avis avis)
        {
            return APIController.PostItem<Avis>(avis, "/Avis/post");
        }
        public static bool PostClients(Clients client)
        {
            return APIController.PostItem<Clients>(client, "/Clients/post");
        }
        public static bool PostCommande(Commande commande)
        {
            return APIController.PostItem<Commande>(commande, "/Commande/post");
        }
        public static bool PostCommandeArticle(CommandeArticle comArt)
        {
            return APIController.PostItem<CommandeArticle>(comArt, "/CommandeArticle/post");
        }
        public static bool PostContact(Contact contact)
        {
            return APIController.PostItem<Contact>(contact, "/Contact/post");
        }
        public static bool PostFamille(Famille famille)
        {
            return APIController.PostItem<Famille>(famille, "/Famille/post");
        }
        public static bool PostGerant(Gerant gerant)
        {
            return APIController.PostItem<Gerant>(gerant, "/Gerant/post");
        }
        public static bool PostProvider(Provider fournisseur)
        {
            return APIController.PostItem<Provider>(fournisseur, "/Provider/post");
        }
        public static bool PostTva(Tva tva)
        {
            return APIController.PostItem<Tva>(tva, "/Tva/post");
        }
        public static bool PostUser(Users user)
        {
            return APIController.PostItem<Users>(user, "/Users/post");
        }

        //Put (update)
        public static bool PutAdress(Adresse adresse, int id)
        {
            return APIController.PutItem<Adresse>(adresse, "/Adresse/put", id);
        }
        public static bool PutAdresseClient(AdresseClient adresseClient, int id)
        {
            return APIController.PutItem<AdresseClient>(adresseClient, "/AdresseClient/put", id);
        }
        public static bool PutArticle(Article article, int id)
        {
            return APIController.PutItem<Article>(article, "/Article/put", id);
        }
        public static bool PutAvis(Avis avis, int id)
        {
            return APIController.PutItem<Avis>(avis, "/Avis/put", id);
        }
        public static bool PutClients(Clients client, int id)
        {
            return APIController.PutItem<Clients>(client, "/Clients/put", id);
        }
        public static bool PutCommande(Commande commande, int id)
        {
            return APIController.PutItem<Commande>(commande, "/Commande/put", id);
        }
        public static bool PutCommandeArticle(CommandeArticle comArt, int id)
        {
            return APIController.PutItem<CommandeArticle>(comArt, "/CommandeArticle/put", id);
        }
        public static bool PutContact(Contact contact, int id)
        {
            return APIController.PutItem<Contact>(contact, "/Contact/put", id);
        }
        public static bool PutFamille(Famille famille, int id)
        {
            return APIController.PutItem<Famille>(famille, "/Famille/put", id);
        }
        public static bool PutGerant(Gerant gerant, int id)
        {
            return APIController.PutItem<Gerant>(gerant, "/Gerant/put", id);
        }
        public static bool PutProvider(Provider fournisseur, int id)
        {
            return APIController.PutItem<Provider>(fournisseur, "/Provider/put", id);
        }
        public static bool PutTva(Tva tva, int id)
        {
            return APIController.PutItem<Tva>(tva, "/Tva/put", id);
        }
        public static bool PutUser(Users user, int id)
        {
            return APIController.PutItem<Users>(user, "/Users/put", id);
        }

    }
}