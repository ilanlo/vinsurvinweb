﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Markup;
using System.Xml.Linq;
// using MySqlX.XDevAPI;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
// using Savage.Data.MySqlClient;
using System.Net.Http;
using System.Net;

namespace VinSurVingtWeb.Controllers
{
    internal class APIController
    {
        private const string baseUrl = @"https://localhost:7110/api";
        public static async Task<List<T>> GetAllReviews<T>(string path)
        {
            //const string baseUrl = @"https://localhost:7110/api";
            string url = baseUrl + path;

            using (HttpClient client = new HttpClient())
            {

                client.BaseAddress = new Uri(url);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = client.GetAsync(url).Result;

                if (!response.IsSuccessStatusCode)
                    throw new Exception();

                string json = response.Content.ReadAsStringAsync().Result.ToString();

                json = json.Remove(0, 1);
                json = json.Remove(json.Length - 1, 1);
                json = Regex.Unescape(json);

                //uncomment next line to print your json results
                // Debug.WriteLine(json);

                List<T> data = null;

                ITraceWriter writer = new MemoryTraceWriter();
                JsonSerializerSettings settings = new JsonSerializerSettings
                {
                    TraceWriter = writer
                };

                try
                {
                    data = JsonConvert.DeserializeObject<List<T>>(json, settings);
                }
                catch (System.Exception ex)
                {
                    Debug.WriteLine("### Exception " + ex.Message);
                    Debug.WriteLine("### Trace Results: " + writer);
                }
                return data;
            }
        }
        public static async Task<List<T>> GetReviewById<T>(string path, int id)
        {
            //const string baseUrl = @"https://localhost:7110/api";
            string url = baseUrl + path + "/" + id;

            using (HttpClient client = new HttpClient())
            {

                client.BaseAddress = new Uri(url);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = client.GetAsync(url).Result;

                if (!response.IsSuccessStatusCode)
                    throw new Exception();

                string json = response.Content.ReadAsStringAsync().Result.ToString();

                json = json.Remove(0, 1);
                json = json.Remove(json.Length - 1, 1);
                json = Regex.Unescape(json);

                //uncomment next line to print your json results
                // Debug.WriteLine(json);

                List<T> data = null;

                ITraceWriter writer = new MemoryTraceWriter();
                JsonSerializerSettings settings = new JsonSerializerSettings
                {
                    TraceWriter = writer
                };

                try
                {
                    data = JsonConvert.DeserializeObject<List<T>>(json, settings);
                }
                catch (System.Exception ex)
                {
                    Debug.WriteLine("### Exception " + ex.Message);
                    Debug.WriteLine("### Trace Results: " + writer);
                }
                return data;
            }
        }
        //overload for search
        public static async Task<List<T>> GetReviewById<T>(string path, string id)
        {
            //const string baseUrl = @"https://localhost:7110/api";
            string url = baseUrl + path + "/" + id;

            using (HttpClient client = new HttpClient())
            {

                client.BaseAddress = new Uri(url);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = client.GetAsync(url).Result;

                if (!response.IsSuccessStatusCode)
                    throw new Exception();

                string json = response.Content.ReadAsStringAsync().Result.ToString();

                json = json.Remove(0, 1);
                json = json.Remove(json.Length - 1, 1);
                json = Regex.Unescape(json);

                //uncomment next line to print your json results
                // Debug.WriteLine(json);

                List<T> data = null;

                ITraceWriter writer = new MemoryTraceWriter();
                JsonSerializerSettings settings = new JsonSerializerSettings
                {
                    TraceWriter = writer
                };

                try
                {
                    data = JsonConvert.DeserializeObject<List<T>>(json, settings);
                }
                catch (System.Exception ex)
                {
                    Debug.WriteLine("### Exception " + ex.Message);
                    Debug.WriteLine("### Trace Results: " + writer);
                }
                return data;

            }
        }

        public static bool PostItem<T>(T json, string url)
        {
            //const string baseUrl = @"https://localhost:7110/api";
            url = baseUrl + url;
            HttpClient _httpClient = new HttpClient();
            using (var content = new StringContent(JsonConvert.SerializeObject(json), System.Text.Encoding.UTF8, "application/json"))
            {
                //Debug.WriteLine(content.ToString);
                HttpResponseMessage result = _httpClient.PostAsync(url, content).Result;
                if (result.StatusCode == System.Net.HttpStatusCode.OK)
                    return true;
                string returnValue = result.Content.ReadAsStringAsync().Result;
                
                throw new Exception($"Failed to POST data: ({result.StatusCode}): {returnValue}");
            }
        }
        public static bool PutItem<T>(T json, string url, int id)
        {
            //const string baseUrl = @"https://localhost:7110/api";
            url = baseUrl + url + "/" + id;
            HttpClient _httpClient = new HttpClient();
            using (var content = new StringContent(JsonConvert.SerializeObject(json), System.Text.Encoding.UTF8, "application/json"))
            {
                //Debug.WriteLine(content.ToString);
                HttpResponseMessage result = _httpClient.PutAsync(url, content).Result;
                if (result.StatusCode == System.Net.HttpStatusCode.OK)
                    return true;
                string returnValue = result.Content.ReadAsStringAsync().Result;
                throw new Exception($"Failed to Put data: ({result.StatusCode}): {returnValue}");
            }

        }
        public static bool DeleteItem(string url, int id)
        {
            //const string baseUrl = @"https://localhost:7110/api";
            url = baseUrl + url + "/" + id;
            HttpClient _httpClient = new HttpClient();
            
            HttpResponseMessage result = _httpClient.DeleteAsync(url).Result;
            if (result.StatusCode == System.Net.HttpStatusCode.OK)
                return true;
            string returnValue = result.Content.ReadAsStringAsync().Result;
            throw new Exception($"Failed to Put data: ({result.StatusCode}): {returnValue}");
        }


    }
}
