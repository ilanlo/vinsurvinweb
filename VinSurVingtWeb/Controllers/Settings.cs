﻿using Microsoft.AspNetCore.Mvc;

namespace VinSurVingtWeb.Controllers
{
    public class settings : Controller
    {
        //public IActionResult Index()
        //{
        //    return View();
        //}

        public LocalRedirectResult loggout() {
            // Check if user is logged in
            if(!string.IsNullOrEmpty(HttpContext.Session.GetInt32("UserId").ToString())) {
                HttpContext.Session.Remove("UserId");
            }

            // Return to home page
            return LocalRedirect("~/");
        }
        
        public LocalRedirectResult orderHistory() {
            return LocalRedirect("~/Home/historique");
        }
    }
}
