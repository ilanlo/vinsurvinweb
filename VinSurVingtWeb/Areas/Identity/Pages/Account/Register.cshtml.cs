﻿// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
#nullable disable

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using VinSurVingtWeb.Controllers;
using VinSurVingtWeb.Models;

namespace VinSurVingtWeb.Areas.Identity.Pages.Account
{
    public class RegisterModel : PageModel
    {
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IUserStore<IdentityUser> _userStore;
        private readonly IUserEmailStore<IdentityUser> _emailStore;
        private readonly ILogger<RegisterModel> _logger;
        private readonly IEmailSender _emailSender;

        public RegisterModel(
            UserManager<IdentityUser> userManager,
            IUserStore<IdentityUser> userStore,
            SignInManager<IdentityUser> signInManager,
            ILogger<RegisterModel> logger,
            IEmailSender emailSender)
        {
            _userManager = userManager;
            _userStore = userStore;
            _emailStore = GetEmailStore();
            _signInManager = signInManager;
            _logger = logger;
            _emailSender = emailSender;
        }

        /// <summary>
        ///     This API supports the ASP.NET Core Identity default UI infrastructure and is not intended to be used
        ///     directly from your code. This API may change or be removed in future releases.
        /// </summary>
        [BindProperty]
        public InputModel Input { get; set; }

        /// <summary>
        ///     This API supports the ASP.NET Core Identity default UI infrastructure and is not intended to be used
        ///     directly from your code. This API may change or be removed in future releases.
        /// </summary>
        public string ReturnUrl { get; set; }

        /// <summary>
        ///     This API supports the ASP.NET Core Identity default UI infrastructure and is not intended to be used
        ///     directly from your code. This API may change or be removed in future releases.
        /// </summary>
        public IList<AuthenticationScheme> ExternalLogins { get; set; }

        /// <summary>
        ///     This API supports the ASP.NET Core Identity default UI infrastructure and is not intended to be used
        ///     directly from your code. This API may change or be removed in future releases.
        /// </summary>
        public class InputModel
        {
            /// <summary>
            ///     This API supports the ASP.NET Core Identity default UI infrastructure and is not intended to be used
            ///     directly from your code. This API may change or be removed in future releases.
            /// </summary>
            [Required]
            [EmailAddress]
            [Display(Name = "Email")]
            public string Email { get; set; }

            /// <summary>
            ///     This API supports the ASP.NET Core Identity default UI infrastructure and is not intended to be used
            ///     directly from your code. This API may change or be removed in future releases.
            /// </summary>
            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }

            /// <summary>
            ///     This API supports the ASP.NET Core Identity default UI infrastructure and is not intended to be used
            ///     directly from your code. This API may change or be removed in future releases.
            /// </summary>
            [DataType(DataType.Password)]
            [Display(Name = "Confirm password")]
            [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }

            [Required]
            [Display(Name = "Lastname")]
            public string Lastname { get; set; }

            [Required]
            [Display(Name = "Firstname")]
            public string Firstname { get; set; }

            [Required]
            [Display(Name = "Phone")]
            public string Phone { get; set; }
            
            [Required]
            [Display(Name = "Address")]
            public string Address { get; set; }

            [Display(Name = "Complement")]
            public string Complement { get; set; }

            [Required]
            [Display(Name = "CP")]
            public string CP { get; set; }

            [Required]
            [Display(Name = "City")]
            public string City { get; set; }
        }


        public async Task OnGetAsync(string returnUrl = null)
        {
            ReturnUrl = returnUrl;
            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl ??= Url.Content("~/");

            if (ModelState.IsValid)
            {
                // Vérifie que l'utilisateur existe pas
                Users newUser;
                try
                {
                    newUser = APIMethods.GetUsersByEmail(Input.Email);
                }
                catch
                {
                    string encodePassword;

                    byte[] encData_byte = new byte[Input.Password.Length];
                    encData_byte = System.Text.Encoding.UTF8.GetBytes(Input.Password);
                    encodePassword = Convert.ToBase64String(encData_byte);

                    // Création du nouvel utilisateur
                    Users nouveauUser = new Users
                    {
                        authMail = Input.Email,
                        authPassword = encodePassword
                    };

                    APIMethods.PostUser(nouveauUser);

                    // Récupération du nouvel utilisateur
                    Users utilisateur = APIMethods.GetUsersByEmail(Input.Email);

                    // Création du client
                    Clients client = new Clients {
                        clientNom = Input.Lastname.ToUpper(),
                        clientPrenom = char.ToUpper(Input.Firstname[0])+ Input.Firstname.Substring(1).ToLower(),
                        clientTel = Input.Phone,
                        authReference = utilisateur.authReference,
                        clientsStatut = 1
                    };

                    APIMethods.PostClients(client);

                    List<Clients> clts = APIMethods.GetAllClients();

                    int clientId = 0;
                    foreach(Clients clt in clts) {
                        if(clt.authReference == utilisateur.authReference) {
                            clientId = clt.clientReference;
                        }
                    }

                    string cmpl = "";
                    if(!string.IsNullOrEmpty(Input.Complement)) {
                        cmpl = Input.Complement;
                    }

                    // Creation de l'adresse
                    Adresse adresse = new Adresse {
                        adresseAdresse = Input.Address,
                        adresseComplement = cmpl,
                        adresseCP = Input.CP,
                        adresseVille = Input.City,
                        adressePays = "France",
                    };

                    APIMethods.PostAdress(adresse);

                    // Récupération de toute les adresses, pour l'identifiant de l'adresse
                    List<Adresse> adrs = APIMethods.GetAllAdress();
                    
                    int adrClient = 0;
                    foreach(Adresse adrse in adrs) {
                        if(adrse.adresseAdresse == Input.Address && adrse.adresseVille == Input.City) {
                            adrClient = adrse.adresseReference;
                        }
                    }

                    // Ajout de l'adresse client
                    AdresseClient adrClt = new AdresseClient {
                        clientReference = clientId,
                        adresseReference = adrClient,
                        typeAdresse = "Livraison"
                    };

                    APIMethods.PostAdresseClient(adrClt);

                    // Enregistrement en mémoire de l'utilisateur en mémoire
                    HttpContext.Session.SetInt32("UserId", utilisateur.authReference);

                    // Envoie vers la page d'accueil
                    return LocalRedirect(returnUrl);
                }

                _logger.LogInformation("Est existant");
                return Page();
            }

            // If we got this far, something failed, redisplay form
            return Page();
        }

        private IdentityUser CreateUser()
        {
            try
            {
                return Activator.CreateInstance<IdentityUser>();
            }
            catch
            {
                throw new InvalidOperationException($"Can't create an instance of '{nameof(IdentityUser)}'. " +
                    $"Ensure that '{nameof(IdentityUser)}' is not an abstract class and has a parameterless constructor, or alternatively " +
                    $"override the register page in /Areas/Identity/Pages/Account/Register.cshtml");
            }
        }

        private IUserEmailStore<IdentityUser> GetEmailStore()
        {
            if (!_userManager.SupportsUserEmail)
            {
                throw new NotSupportedException("The default UI requires a user store with email support.");
            }
            return (IUserEmailStore<IdentityUser>)_userStore;
        }
    }
}
