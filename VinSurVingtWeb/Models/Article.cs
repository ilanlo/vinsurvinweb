﻿using Newtonsoft.Json;

namespace VinSurVingtWeb.Models
{
    public class Article
    {
        [JsonProperty("articleReference")]
        public int articleReference { get; set; }

        [JsonProperty("articleResignation")]
        public string articleResignation { get; set; }

        [JsonProperty("articlePrixUnitaire")]
        public double articlePrixUnitaire { get; set; }

        [JsonProperty("articlePrixCarton")]
        public double articlePrixCarton { get; set; }

        [JsonProperty("articlePrixFournisseur")]
        public double articlePrixFournisseur { get; set; }

        [JsonProperty("articleAnnee")]
        public int articleAnnee { get; set; }

        [JsonProperty("articleQuantite")]
        public int articleQuantite { get; set; }

        [JsonProperty("articleDegre")]
        public double articleDegre { get; set; }

        [JsonProperty("articleDescription")]
        public string articleDescription { get; set; }

        [JsonProperty("tvaReference")]
        public int tvaReference { get; set; }

        [JsonProperty("familleReference")]
        public int familleReference { get; set; }

        [JsonProperty("fournisseurReference")]
        public int fournisseurReference { get; set; }

    }
}
