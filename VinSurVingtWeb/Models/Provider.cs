﻿using Newtonsoft.Json;

namespace VinSurVingtWeb.Models
{
    public class Provider
    {
        [JsonProperty("fournisseurReference")]
        public int fournisseurReference { get; set; }

        [JsonProperty("fournisseurNom")]
        public string fournisseurNom { get; set; }

        [JsonProperty("fournisseurPrenom")]
        public string fournisseurPrenom { get; set; }

        [JsonProperty("fournisseurMail")]
        public string fournisseurMail { get; set; }

        [JsonProperty("fournisseurTel")]
        public string fournisseurTel { get; set; }

        [JsonProperty("adresseReference")]
        public int adresseReference { get; set; }

        [JsonProperty("fournisseurSociete")]
        public string fournisseurSociete { get; set; }

        [JsonProperty("fournisseurStatut")]
        public int fournisseurStatut { get; set; }
    }
}
