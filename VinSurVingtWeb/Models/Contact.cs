﻿using Newtonsoft.Json;

namespace VinSurVingtWeb.Models
{
    public class Contact
    {
        [JsonProperty("conReference")]
        public int conReference { get; set; }

        [JsonProperty("conMsg")]
        public string conMsg { get; set; }

        [JsonProperty("authReference")]
        public int authReference { get; set; }

        [JsonProperty("conDate")]
        public string conDate { get; set; }
    }
}
