﻿using Newtonsoft.Json;

namespace VinSurVingtWeb.Models
{
    public class AdresseClient
    {
        [JsonProperty("clientReference")]
        public int clientReference { get; set; }

        [JsonProperty("adresseReference")]
        public int adresseReference { get; set; }

        [JsonProperty("typeAdresse")]
        public string typeAdresse { get; set; }

    }
}
