﻿namespace VinSurVingtWeb.Models
{
    public class HomeViewModel
    {

        public List<Famille> FamilleList { get; set; }
        public List<Article> ArticleList { get; set; }
    }
}
