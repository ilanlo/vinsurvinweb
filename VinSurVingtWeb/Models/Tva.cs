﻿using Newtonsoft.Json;

namespace VinSurVingtWeb.Models
{
    public class Tva
    {
        [JsonProperty("tvaReference")]
        public int tvaReference { get; set; }

        [JsonProperty("tvaTva")]
        public float tvaTva { get; set; }
    }
}
