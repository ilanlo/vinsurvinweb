﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VinSurVingtWeb.Models
{
    public class Users
    {
        [JsonProperty("authReference")]
        public int authReference { get; set; }

        [JsonProperty("authMail")]
        public string authMail { get; set; }

        [JsonProperty("authPassword")]
        public string authPassword { get; set; }
    }
}
