﻿using Microsoft.VisualBasic;
using Newtonsoft.Json;

namespace VinSurVingtWeb.Models
{
    public class Commande
    {
        [JsonProperty("commandeReference")]
        public int commandeReference { get; set; }

        [JsonProperty("commandeDate")]
        public string commandeDate { get; set; }

        [JsonProperty("commandeType")]
        public string commandeType { get; set; }

        [JsonProperty("commandePrix")]
        public double commandePrix { get; set; }

        [JsonProperty("fournisseurReference")]
        public int? fournisseurReference { get; set; }

        [JsonProperty("clientReference")]
        public int? clientReference { get; set; }

    }
}
