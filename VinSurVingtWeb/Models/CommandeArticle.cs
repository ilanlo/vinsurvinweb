﻿using Newtonsoft.Json;

namespace VinSurVingtWeb.Models
{
    public class CommandeArticle
    {
        [JsonProperty("articleReference")]
        public int articleReference { get; set; }

        [JsonProperty("commandeReference")]
        public int commandeReference { get; set; }

        [JsonProperty("quantité")]
        public int quantité { get; set; }


    }
}
