﻿// using MailKit.Search;
using Newtonsoft.Json;
// using System.DirectoryServices;
// using System.Runtime.Serialization;

namespace VinSurVingtWeb.Models
{
    public class Clients
    {
        [JsonProperty("clientReference")]
        public int clientReference { get; set; }

        [JsonProperty("clientNom")]
        public string clientNom { get; set; }

        [JsonProperty("clientPrenom")]
        public string clientPrenom { get; set; }

        [JsonProperty("clientTel")]
        public string clientTel { get; set; }

        [JsonProperty("authReference")]
        public int authReference { get; set; }

        [JsonProperty("clientsStatut")]
        public int clientsStatut { get; set; }

    }
}
