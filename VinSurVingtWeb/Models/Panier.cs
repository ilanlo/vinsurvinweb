﻿using Microsoft.AspNetCore.Mvc;
using VinSurVingtWeb.Controllers;
using System.Web;

namespace VinSurVingtWeb.Models;


public class Panier
{
    // GET
    // public IActionResult Index()
    // {
    //     return View();
    // }

    /// <summary>
    ///     Méthode d'ajout un élément dans le panier.
    /// </summary>
    /// <param name="idArticle"></param>
    /// <param name="qt"></param>
    public void nouvelArticle(int idArticle, int qt)
    {
        // Récupération des articles du panier
        IHttpContextAccessor accessor = new HttpContextAccessor();
        string articles;
        string[] identifiantArticles = new string[] { };
        
        if (!string.IsNullOrEmpty(accessor.HttpContext.Session.GetString("panier")))
        {
            articles = accessor.HttpContext.Session.GetString("panier");
            identifiantArticles = articles.Split("=");
        }

        // Création de la nouvelle valeur du panier
        string dataPanier = "";
        bool estExistant = false;

        // Parcourt le tableau d'article en cours 
        foreach (string articleEnCours in identifiantArticles)
        {
            // Vérifie si l'article est existant dans le panier
            if (articleEnCours.Split("-")[0] == idArticle.ToString())
            {
                dataPanier += idArticle.ToString() + "-" + qt.ToString()+ "=";
                estExistant = true;
            }
            else
            {
                dataPanier += articleEnCours + "=";
            }
        }

        if (!estExistant)
        {
            dataPanier += idArticle.ToString() + "-" + qt.ToString()+ "=";
        }

        // Vérifie si le panier est vide
        if (string.IsNullOrEmpty(dataPanier))
        {
            dataPanier = idArticle.ToString() + "-" + qt.ToString()+ "=";
        }
        
        // Set le nouveau panier
        accessor.HttpContext.Session.SetString("panier", dataPanier);

        return;
    }

    /// <summary>
    ///     Méthode pour retirer un article du panier
    /// </summary>
    /// <param name="idArticle"></param>
    public static void retierArticle(int idArticle)
    {
        // Récupération des articles du panier
        IHttpContextAccessor accessor = new HttpContextAccessor();
        string articles;
        string[] identifiantArticles = new string[] { };
        
        if (!string.IsNullOrEmpty(accessor.HttpContext.Session.GetString("panier")))
        {
            articles = accessor.HttpContext.Session.GetString("panier");
            identifiantArticles = articles.Split("=");
        }

        string dataPanier = "";
        
        // Parcourt le tableau d'article en cours 
        foreach (string articleEnCours in identifiantArticles)
        {
            // Vérifie si l'article est existant dans le panier
            if (articleEnCours.Split("-")[0] != idArticle.ToString())
            {
                dataPanier += articleEnCours + "=";
            }
        }
        
        // Set le nouveau panier
        accessor.HttpContext.Session.SetString("panier", dataPanier);

        return;
        
    }
    
    public void valider() {}

    /// <summary>
    ///     Méthode de récupération des articles dans le panier
    /// </summary>
    /// <returns></returns>
    public string[] recupererPanier()
    {
        // Récupération des articles du panier
        IHttpContextAccessor accessor = new HttpContextAccessor();
        string articles;
        string[] identifiantArticles = new string[] { };;
        
        if (!string.IsNullOrEmpty(accessor.HttpContext.Session.GetString("panier")))
        {
            articles = accessor.HttpContext.Session.GetString("panier");
            identifiantArticles = articles.Split("=");
        }

        // Retourne la liste du panier
        return identifiantArticles;
    }

    public double prixTotal()
    {
        // Initialisation du prix
        double prix = 0;

        // Récupération du panier
        string[] panier = recupererPanier();

        // Parcourt la liste des artciles
        for(int i = 0; i < panier.Length-1; i++)
        {
            string article = panier[i];
            if (!string.IsNullOrEmpty(article.Split("-")[0]))
            {
                // Récupération du prix des articles
                Article art = APIMethods.GetArticleById(Convert.ToInt32(article.Split("-")[0]));

                int qt = Convert.ToInt32(article.Split("-")[1]);

                double prixArticle = art.articlePrixUnitaire;

                List<Tva> tva = APIMethods.GetAllTva();

                // Calcul du prix totale
                prix += Math.Round((prixArticle + (prixArticle * (tva[0].tvaTva / 100))) * qt, 2);
            }
        }

        return prix;
    }	
}