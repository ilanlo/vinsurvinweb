﻿using Newtonsoft.Json;

namespace VinSurVingtWeb.Models
{
    public class Famille
    {
        [JsonProperty("familleReference")]
        public int familleReference { get; set; }

        [JsonProperty("familleNom")]
        public string familleNom { get; set; }
    }
}
