﻿using Newtonsoft.Json;

namespace VinSurVingtWeb.Models
{
    public class Adresse
    {
        [JsonProperty("adresseReference")]
        public int adresseReference { get; set; }

        [JsonProperty("adresseAdresse")]
        public string adresseAdresse { get; set; }

        [JsonProperty("adresseComplement")]
        public string adresseComplement { get; set; }

        [JsonProperty("adresseCP")]
        public string adresseCP { get; set; }

        [JsonProperty("adresseVille")]
        public string adresseVille { get; set; }

        [JsonProperty("adressePays")]
        public string adressePays { get; set; }
    }
}
