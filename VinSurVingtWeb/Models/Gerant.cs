﻿using Newtonsoft.Json;

namespace VinSurVingtWeb.Models
{
    public class Gerant
    {
        [JsonProperty("gerantReference")]
        public int gerantReference { get; set; }

        [JsonProperty("authReference")]
        public int authReference { get; set; }
    }
}
