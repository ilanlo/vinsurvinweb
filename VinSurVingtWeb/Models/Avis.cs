﻿using Newtonsoft.Json;

namespace VinSurVingtWeb.Models
{
    public class Avis
    {
        [JsonProperty("avisReference")]
        public int avisReference { get; set; }

        [JsonProperty("avisNote")]
        public int avisNote { get; set; }

        [JsonProperty("avisDescription")]
        public string avisDescription { get; set; }

        [JsonProperty("avisTitre")]
        public string avisTitre { get; set; }

        [JsonProperty("clientReference")]
        public int clientReference { get; set; }

        [JsonProperty("articleReference")]
        public int articleReference { get; set; }

    }
}
