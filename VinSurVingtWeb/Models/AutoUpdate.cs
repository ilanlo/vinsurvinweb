﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VinSurVingtWeb.Models
{
    public class AutoUpdate
    {
        [JsonProperty("id")]
        public int id { get; set; }

        [JsonProperty("isActive")]
        public bool isActive { get; set; }

        [JsonProperty("threshold")]
        public int threshold { get; set; }
    }
}
