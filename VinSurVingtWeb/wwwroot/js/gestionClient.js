/**
 * ## Centrer les élément
 * Cette fonction est appelée au chargement de la page Login.  
 * Elle permet de centrer les éléments au centre de la page, sur l'axe X et Y.
 */
function elementCenter() {
    // Le body
    const body = document.querySelector('body');
    body.style.marginBottom = `0`;
    body.style.height = `100vh`;
    // La div qui suit
    document.querySelector('#mainElement').style.height = `70%`;
    // Récupération du mail à son tour
    document.querySelector('main').style.height = `100%`;
}